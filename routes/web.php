<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


include_once "route2.php";

$router->get('/key', function() {
    return str_random(32);
});


$router->get('/', function () use ($router) {
//    return $router->app->version();
    return 'Selamat datang di Aplikasi Pas ni!';
});
