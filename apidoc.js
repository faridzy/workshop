/**
* @api {post} /shop/create create shop
* @apiParam {integer} users_id user id
* @apiParam {string} name name of shop
* @apiParam {string} description description of shop
* @apiParam {string} address address of shop
* @apiParamExample {object} example :
*   {
*       users_id    : 1,
*       name        : 'udin shop',
*       description : 'toko hebat',
*       address     : 'jl mana aja'
*   }
* @apiSuccess {boolean} success true or false
**/

/**
* @api {post} /product/create create product
* @apiParam {integer} users_id user id
* @apiParam {integer} subcategories_id sub category id
* @apiParam {string} name name of product
* @apiParam {file} image image of product
* @apiParam {integer} price price of product
* @apiParam {integer} stock unit of product
* @apiParam {string} location label of product
* @apiParam {string} description description of product
* @apiParamExample {object} example :
*   {
*       shop_id             : 1,
*       subcategories_id    : 1,
*       image               : 'image.jpg',
*       price               : 20000,
*       stock               : 1000,
*       location            : 'malang',
*       description         : 'bayam maknyus',
*   }
* @apiSuccess {boolean} success true or false
**/

/**
* @api {post} /product/delete/{id_product} delete product
* @apiSuccess {boolean} success true or false
**/

/**
* @api {post} /register Register user
* @apiParam {string} email
* @apiParam {string} password
* @apiParam {string} confirm_password
* @apiParam {string} first_name
* @apiParam {string} last_name
* @apiParam {string} access
 * @apiParamExample {object} example :
 *   {
 *      email               : user@pasni.com
 *      password            : user123
 *      confirm_password    : user123
 *      first_name          : Margono
 *      last_name           : Sumarno
 *      access              : admin
 *   }
 *  @apiSuccess {boolean} success true or false
**/
/**
* @api {post} /register Register Courier
* @apiParam {string} email
* @apiParam {string} password
* @apiParam {string} confirm_password
* @apiParam {string} first_name
* @apiParam {string} last_name
 * @apiParamExample {object} example :
 *   {
 *      email               : user@pasni.com
 *      password            : user123
 *      confirm_password    : user123
 *      first_name          : Margono
 *      last_name           : Sumarno
 *   }
 *  @apiSuccess {boolean} success true or false
 *  @apiSuccess {string} message Berhasil disimpan !
**/

/**
* @api {post} /login Login user
* @apiParam {string} email
* @apiParam {string} password
* @apiParam {string} grant_type
* @apiParam {string} client_id
* @apiParam {string} client_secret
 * @apiParamExample {object} example :
 *   {
 *      email               : user@pasni.com
 *      password            : user123
 *      grant_type          : asd
 *      client_id           : asdasdsadsd
 *      client_secret       : asdasdasdasd
 *   }
 *  @apiSuccess {boolean} success true or false
 *  @apiSuccess {string} token_type Baerer
 *  @apiSuccess {string} access_token access token
 *  @apiSuccess {object} data access token
**/

/**
* @api {get} /admin/get-admin get all Admin member
**/
/**
* @api {get} /admin/get-cs get all CS member
**/
/**
* @api {get} /services/category/get get category only
**/
/**
* @api {get} /services/category/get/all get all category and subcategory
**/
/**
* @api {get} /services/subcategory/get/ get all category and subcategory
**/
/**
* @api {post} /services/product/buy buy product
 * @apiParam {integer} product_id
 * @apiParam {integer} users_id
 * @apiParam {integer} stock
**/
/**
* @api {get} /services/cart/get/{users_id} getCart
**/
/**
* @api {get} /services/cart/checkout/{users_id} checkout
**/
/**
* @api {get} /services/product/view/0 product before acc
**/
