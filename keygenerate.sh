#!/bin/bash
if [ "$1" != "" ]; then

#setup default directory
cd /home/user/web

lumen new $1

cd $1

cp .env.example .env

key=`php -r "echo md5(uniqid()).\"\n\";"`;

sed -i "s/APP_KEY=SomeRandomKey!!!/APP_KEY=$key/" .env

fi
