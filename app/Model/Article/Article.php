<?php
namespace App\Model\Article;
use App\Model\CategoryArticle\CategoryArticle;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
 class Article extends Model
 {
 	
 	protected $table='article';
 	protected $fillable=[
 		'judul',
 		'isi',
 		'category_article_id',

 	];

 	public static $validation_rules = [
        'judul' => 'required',
       
    ];
    
    public function categoryArticle(){
    	 return $this->hasOne(CategoryArticle::class,'category_article_id','id');
    }

    
 } 