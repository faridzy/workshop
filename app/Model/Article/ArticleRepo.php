<?php
namespace App\Model\Article;

use App\Model\BaseRepo;

/**
* 
*/
class Article extends BaseRepo
{
	
	 public function __construct(Article $article)
    {
        $this->model = $article;
    }
    public function searchAll($search)
    {
        return $this->model
            ->orWhere('name','like','%'.$search.'%')
            ->with([
                'category_article' => function($query) use ($search){
                }
            ])
            ->get();
    }
    public function getDataAll(){
        $data = $this->model
            ->with(['categoryArticle'])
            ->get();
        return $data;
    }
}