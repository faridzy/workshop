<?php
namespace App\Model\CategoryArticle;
use Illuminate\Database\Eloquent\Model;

/**
* 
*/
class CategoryArticle extends Model
{
	protected $table='categories_article';

	protected $fillable=[
		'name'];

	public static $validation_rules=[
		'name'=>'required'
	];	
	
}