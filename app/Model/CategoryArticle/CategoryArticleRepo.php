<?php

namespace App\Model\CategoryArticle;
use App\Model\BaseRepo;

/**
* 
*/
class CategoryArticleRepo extends BaseRepo
{
	public function __construct(CategoryArticle $categoryArticle){

		$this->model = $categoryArticle;

	}

	public function getDataAll(){
		$data=$this->model->get();
		return $data;
		
	}
	
	
}