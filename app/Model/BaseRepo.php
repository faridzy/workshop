<?php 
namespace App\Model;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class BaseRepo
{
    protected $model;
    protected $relations;

    public function __construct($model = null)
    {
        $this->model = $model;
    }
    public function input($input, $role)
    {
        $validate_user  = Validator::make($input, $role);
        if ($validate_user->fails()) {
            return response([
                'success'     => false,
                'message'    => 'Validasi gagal. Cek kembali form anda !'
            ]);
        }

        $input['created_at'] = Carbon::now()->toDateTimeString();
        $input['updated_at'] = Carbon::now()->toDateTimeString();
        $proses = $this->model->insert($input);
        if($proses)
            return response(['success'  => true,]);
        return response(['success'  => false,]);
    }
    public function update($input,$role,$id)
    {
        $validate_user  = Validator::make($input, $role);
        if ($validate_user->fails()) {
            return response([
                'success'     => false,
                'message'    => 'Validasi gagal. Cek kembali form anda !'
            ]);
        }
        $input['updated_at'] = Carbon::now()->toDateTimeString();
        $proses = $this->model->where('id','=',$id)
            ->update($input);
        if($proses)
            return response(['success'  => true,]);
        return response(['success'  => false,]);
    }

    public function getData()
    {
        $proses = $this->model->select('*')
            ->orderBy('id','desc')
            ->get();
        return $proses;
    }
    public function detail($id)
    {
        return $this->model->find($id);
    }
    public function getDataWhere($kolom,$value)
    {
        $proses = $this->model
            ->where($kolom,'=',$value)
            ->get();
        return $proses;
    }
    public function getDataWhereWith($kolom,$value, $with = [])
    {
        $proses = $this->model
            ->with($with)
            ->where($kolom,'=',$value)
            ->get();
        return $proses;
    }
    public function getDataWhere2($kolom,$value,$kolom2,$value2)
    {
        $proses = $this->model
            ->where($kolom,'=',$value)
            ->where($kolom2,'=',$value2)
            ->get();
        return $proses;
    }
    public function delete($id)
    {
        $res = $this->model->where('id','=',$id)
            ->delete();
        return true;
    }
    public function deleteWhere($kolom,$value)
    {
        $res = $this->model
            ->where($kolom,'=',$value)
            ->delete();
        return true;
    }

    public function getPaginate($limit = 10)
    {
        $data = $this->model
            ->orderBy('id','desc')
            ->paginate($limit);
        return $data;
    }
    public function getDataLimit($limit)
    {
        $data = $this->model
            ->orderBy('id','desc')
            ->limit($limit)
            ->get();
        return $data;
    }
    public function search($category,$search)
    {
        return $this->model->select('*')
            ->where($category,'like','%'.$search.'%')
            ->get();
    }
    public function sort($kolom,$method)
    {
        return $this->model
            ->orderBy($kolom,$method)
            ->get();
    }

}
