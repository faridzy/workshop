<?php 
namespace App\Model;


use App\Model\CategoryArticle\CategoryArticle;
use App\Model\CategoryArticle\CategoryArticleRepo;
use App\Model\DetailUser\Article;
use App\Model\DetailUser\ArticleRepo;
use Illuminate\Support\ServiceProvider;

class ModelServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
      
        $this->app->bind('article_categories',function(){
            return new CategoryArticleRepo(
                new CategoryArticle()
            );
        });

    }
}