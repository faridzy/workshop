<?php

namespace App\Http\Middleware;

use App\Model\Users\Users;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->header('Authorization') && $request->bearerToken()) {
            $key    = explode(' ', $request->header('Authorization'));
            $user   = Users::with('detailUser')->where('remember_token', $key[1])->first();

            if (!$user) {
                return response([
                    'status'    => 'failed',
                    'message'   => 'Unauthorized access !'
                ], 401);
            }

            $usr['id']              = $user->id;
            $usr['access']          = $user->access;
            $usr['remember_token']  = $user->remember_token;

            if (!empty($user)) {
                $request->request->add([
                    'user'      => (object) $usr
                ]);
            } else {
                return response([
                    'status'    => 'failed',
                    'message'   => 'Unauthorized access !'
                ], 401);
            }
        } else {
            return response([
                'status'    => 'failed',
                'message'   => 'Unauthorized access !'
            ], 401);
        }

        return $next($request);
//        return response($request->all());
    }
}
