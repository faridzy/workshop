<?php

/**
 * @Author: ELL
 * @Date:   2017-10-22 08:45:41
 * @Last Modified by:   ELL
 * @Last Modified time: 2017-10-22 12:37:55
 */

namespace App\Http\Controllers\Services\Article;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\CategoryArticle\CategoryArticle;

/**
* 
*/
class ArticleCategoriesController extends Controller
{
	
	public function getData(){
		return response()->json(app('article_categories')->getData());
	}

	public function create(Request $request){
		$formInput=$request->all();
		$role=CategoryArticle::$validation_rules;

		$input=app('article_categories')->input($formInput,$role);
		if($input)
			return response()->json(['success'=>true]);
		return response()->json(['success'=>false]);

	}

	public function update(Request $request,$id){
		
		
	}
	public function delete($articlecategories_id){
		
	}

}