<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/08/17
 * Time: 14:22
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomController extends Controller
{
    public function getInfo(Request $request) {
        return response($request->server());
    }

    public function getOauthClient(Request $request) {
        $data   = DB::table('oauth_clients')->where('id', 2)->first();

        return response()->json($data);
    }

    public function test(Request $request) {
            return response()->json($request);
    }
}