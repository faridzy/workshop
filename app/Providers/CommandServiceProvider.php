<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 07/09/17
 * Time: 9:47
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Console\Commands\KeyGenerator;

class CommandServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('', function()
        {
            return new KeyGenerator;
        });

        $this->commands(
            'command.my.command'
        );
    }
}